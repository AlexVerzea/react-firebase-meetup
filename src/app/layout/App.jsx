import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

// Components
import EventsPage from '../../features/Events/EventsPage/EventsPage';
import EventDetailsPage from '../../features/Events/EventDetailsPage/EventDetailsPage';
import OrganizersPage from '../../features/Organizers/OrganizersPage';
import EventForm from '../../features/Events/EventDetailsPage/EventForm/EventForm';
import SplashPage from '../../features/Splash/SplashPage';
import NavBar from '../../features/Navigation/NavBar/NavBar';

// Helpers
import { ThemeProvider } from "@callstack/react-theme-provider";
import { themes } from '../../helpers/themes';
import { chooseNextTheme } from '../../helpers/ThemeSelector';
import { events } from '../../helpers/FakeData/events';
import { updates } from '../../helpers/FakeData/updates';

/** Snippets
 * imd: Import snippet.
 * rcc: React Class Component. Smart Component, has access to React Lifecycle Hooks.
 * rfe: React Functional Component. Presentation Component, doesn't have access to React Lifecycle Hooks. Used for Routing.
 */

class App extends Component {

  state = {
    theme: themes.blue
  };
  onChangeTheme = (themeName) => {
    this.setState({ theme: themes[themeName] });
  };

  render() {
    return (
      <ThemeProvider theme={this.state.theme}>
        <BrowserRouter>
          <div style={ this.state.theme.backgroundColor }>
            <Switch>
              <Route exact path='/' render={(routeProps) => (
                <SplashPage 
                  {...routeProps}  
                  themes={Object.keys(themes)}
                  onChangeTheme={this.onChangeTheme}
                  chooseNextTheme={chooseNextTheme}
                />
              )}/> 
            </Switch>
            <Route path='/(.+)' render={() => ( // Route with / + anything else will go here.
              <div>
                <NavBar
                  themes={Object.keys(themes)}
                  onChangeTheme={this.onChangeTheme}
                  chooseNextTheme={chooseNextTheme}
                />
                <Switch>
                  <Route exact path='/events' render={(routeProps) => (
                    <EventsPage 
                      {...routeProps}
                      events={events}  
                      themes={Object.keys(themes)}
                      onChangeTheme={this.onChangeTheme}
                      chooseNextTheme={chooseNextTheme}
                    />
                  )}/>
                  <Route path='/event/:id' component={EventDetailsPage}/>
                  <Route exact path='/organizers' render={(routeProps) => (
                    <OrganizersPage 
                    {...routeProps}  
                    updates={updates}
                    themes={Object.keys(themes)}
                    onChangeTheme={this.onChangeTheme}
                    chooseNextTheme={chooseNextTheme}
                    />
                  )}/>
                  <Route path='/createEvent' component={EventForm}/>
                </Switch>
              </div>
            )}/>
          </div>
        </BrowserRouter>
      </ThemeProvider>
    );
  }
}

export default App;