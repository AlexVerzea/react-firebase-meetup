import React, { Component } from 'react';
import { withTheme } from "@callstack/react-theme-provider";
import { NavLink } from 'react-router-dom';
import { getFullDate, formatTimeSince } from "../../../helpers/TimestampOperations";
import { Card } from 'mdbreact';

class UpdateCard extends Component {
  render() {
    return (
      <Card className="update-card" style={ this.props.theme.primaryGradientColor }>
        <img className="pull-left rounded-circle update-person-portrait" src={ this.props.updateData.photo_url } alt=""/>
        <div className="update-text">
          <NavLink to={ this.props.updateData.organizer.link }>
            <p 
              className="text-left update-person-name"
              style={ this.props.theme.secondaryColor }
            >{ this.props.updateData.organizer.full_name }</p>
          </NavLink>
          <p className="update-description-1 text-left">{ this.props.updateData.action }</p>
          <NavLink to={ this.props.updateData.target.link }>
            <p 
              className="update-description-2 text-left"
              style={ this.props.theme.secondaryColor }
            >{ this.props.updateData.target.text }</p>
          </NavLink>
          <p className="update-description-1 text-left">{ formatTimeSince(getFullDate(this.props.updateData.date)) }{ this.props.updateData.punctuation }</p>
        </div>
      </Card>
    )
  }
}

export default withTheme(UpdateCard);
