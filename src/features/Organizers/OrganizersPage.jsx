import React, { Component } from 'react';
import SectionCard from '../Section/SectionCard';
import { withTheme } from "@callstack/react-theme-provider";
import UpdateCard from './Update/UpdateCard';
import { sortTimestampItems } from "../../helpers/TimestampOperations";

class OrganizersPage extends Component {
  render() {
    return (
      <div className="container-fluid top-margin-adjustment">
        <div className="row">
            <div className="col-lg-6 col-12">
              <SectionCard 
                type="section-card-1" 
                title="Total Organizers:&nbsp;"
                icon="fa-group"
                amount="6"
              />
            </div>
            <div className="col-lg-6 col-12">
              <SectionCard 
                type="section-card-1" 
                title="Total Updates:&nbsp;"
                icon="fa-history"
                amount="6"
              />
              {sortTimestampItems(this.props.updates, "LIFO").map((update, i) => 
                <UpdateCard updateData={update} key={i} />
              )}
            </div>
          </div>
      </div>
    )
  }
}

export default withTheme(OrganizersPage);


