import React, { Component } from 'react';
import SectionCard from '../../Section/SectionCard';
import { withTheme } from "@callstack/react-theme-provider";
import EventCard from './EventCard/EventCard';
import { sortTimestampItems } from "../../../helpers/TimestampOperations";

class EventsPage extends Component {
  render() {
    return (
      <div className="container-fluid top-margin-adjustment">
        <div className="row">
          <div className="col-lg-6 col-12">
            <SectionCard 
              type="section-card-1" 
              title="Total Events:&nbsp;"
              icon="fa-calendar"
              amount="6"
            />
          </div>
          <div className="col-lg-6 col-12">
            <SectionCard 
              type="section-card-1" 
              title="Total Attendees:&nbsp;"
              icon="fa-group"
              amount="6K+"
            />
          </div>
        </div>
        <div className="row">
            {sortTimestampItems(this.props.events, "FIFO").map((event, i) => 
              <div className="col-lg-6 col-12" key={i}>
                <EventCard eventData={event} key={i} />
              </div>
            )}
        </div>
      </div>
    )
  }
}

export default withTheme(EventsPage);
