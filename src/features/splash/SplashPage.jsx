import React from 'react';
import { withTheme } from "@callstack/react-theme-provider";

const SplashPage = ({ history, theme, themes, onChangeTheme, chooseNextTheme }) => {
  return (
    <div className="background-splash container-fluid" style={ theme.backgroundImage }>
      <div className="row">
        <div className="col-12 text-center">
          <h1 style={ theme.primaryTransparentColor }>
            <i className="fa fa-meetup logo" aria-hidden="true"></i>React Firebase Meetup
          </h1>
        </div>
      </div>
      <div className="row">
        <div className="col-12 text-center">
          <button
            style={ theme.primaryTransparentColor }
            className="button-1" 
            onClick={() => history.push('/events')}
          >
            <i className="fa fa-calendar logo" aria-hidden="true"></i>Events
          </button>
          <button
            style={ theme.primaryTransparentColor }
            className="button-1" 
            onClick={() => history.push('/organizers')}
          >
            <i className="fa fa-group logo" aria-hidden="true"></i>People
          </button>
          <button
            style={ theme.primaryTransparentColor }
            className="button-1" 
            onClick={() => history.push('/events')}
          >
            <i className="fa fa-key logo" aria-hidden="true"></i>Sign In
          </button>
          <button
            style={ theme.primaryTransparentColor }
            className="button-1" 
            onClick={() => history.push('/events')}
          >
            <i className="fa fa-sign-in logo" aria-hidden="true"></i>Sign Up
          </button>
          <button
            style={ theme.primaryTransparentColor }
            className="button-1" 
            onClick={() => onChangeTheme(chooseNextTheme(theme.themeName, themes)) }
          >
            <i className="fa fa-paint-brush logo" aria-hidden="true"></i>Theming
          </button>
        </div>
      </div>
    </div>
  )
}

export default withTheme(SplashPage);
