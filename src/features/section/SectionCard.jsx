import React, { Component } from 'react';
import { Card, CardBody } from 'mdbreact';
import { withTheme } from "@callstack/react-theme-provider";

class SectionCard extends Component {
  render() {
    return (
        <Card className={this.props.type} style={ this.props.theme.primaryGradientColor }>
          <CardBody>
            <div className="row">
              <div className="col-12">
                <i className={`fa ${this.props.icon} section-card-logo pull-left`} aria-hidden="true"></i>
                <h4 className="section-card-title pull-left">{this.props.title}</h4> 
                <h4 
                  className="section-card-amount"
                  style={ this.props.theme.secondaryColor }
                >{this.props.amount}</h4> 
              </div>
            </div>
          </CardBody>
        </Card>
    )
  }
}

export default withTheme(SectionCard);
