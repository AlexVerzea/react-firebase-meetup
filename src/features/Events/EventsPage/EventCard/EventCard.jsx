import React, { Component } from 'react';
import { Card, CardBody } from 'mdbreact';
import EventOrganizer from './EventOrganizer';
import { withTheme } from "@callstack/react-theme-provider";
import { months, getFullDate, formatDate, formatTime } from "../../../../helpers/TimestampOperations";

class EventCard extends Component {
  render() {

    const fullDate = getFullDate(this.props.eventData.date);
    const eventData = {
      backgroundImage: {
        backgroundImage: 'url(' + this.props.eventData.image_url + ')'
      },
      time: {
        day: fullDate.getDate(),
        month: months[fullDate.getMonth()],
        formattedDate: formatDate(fullDate),
        formattedTime: formatTime(fullDate)
      },
      organizer: {
        full_name: this.props.eventData.organizer.full_name,
        link: this.props.eventData.organizer.link,
        photo_url: this.props.eventData.organizer.photo_url,
        facebook: this.props.eventData.organizer.facebook,
        twitter: this.props.eventData.organizer.twitter,
        linkedin: this.props.eventData.organizer.linkedin
      }
    };

    return (
      <Card className="event-card" style={ this.props.theme.primaryGradientColor }>
        <CardBody>
          <div className="row">
            <div className="col-12 col-md-6">
              <div style={eventData.backgroundImage} className="event-image">
                <div className="row">
                  <div className="col-lg-12">
                    <div 
                      className="event-date" 
                      aria-hidden="true"
                      style={ this.props.theme.primaryBackgroundColor }
                    >
                      <span className="event-day">
                        <span>{eventData.time.day}</span>
                      </span>
                      <span 
                        className="event-month"
                        style={ this.props.theme.secondaryColor }
                      >
                        <span>{eventData.time.month}</span>
                      </span>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-12">
                    <div 
                      className="event-organizer" 
                      aria-hidden="true"
                      style={ this.props.theme.primaryBackgroundColor }
                    ><EventOrganizer organizerData={eventData.organizer}/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12 col-md-6">
              <div className="row">
                <div className="col-12">
                  <h4 className="event-title">{ this.props.eventData.title }</h4>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <ul className="event-details">
                    <li className="list-inline-item">
                      <i className="fa fa-map-marker" style={ this.props.theme.secondaryColor }></i> { this.props.eventData.location }
                    </li><br/>
                    <li className="list-inline-item">
                      <i className="fa fa-calendar-check-o" style={ this.props.theme.secondaryColor }></i>&nbsp;
                      { eventData.time.formattedDate }, { eventData.time.formattedTime }
                    </li><br/>
                  </ul>
                </div>
              </div>
              <div className="row">
                <div className="col-12 event-description">
                  <p className="multiline-truncation-ellipsis">{ this.props.eventData.description }</p>
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <button 
                    className="button-4" 
                    style={ this.props.theme.cardButton }
                  >Read More</button>
                </div>
              </div>
            </div>
          </div>
        </CardBody>
      </Card>
    )
  }
}

export default withTheme(EventCard);

/* <ul className="event-attendee-list">
                    <li className="list-inline-item"><EventAttendee/></li>
                    <li className="list-inline-item"><EventAttendee/></li>
                    <li className="list-inline-item"><EventAttendee/></li>
                    <li className="list-inline-item"><EventAttendee/></li>
                    <li className="list-inline-item"><div className="number-circle">9+</div></li>
                  </ul> */