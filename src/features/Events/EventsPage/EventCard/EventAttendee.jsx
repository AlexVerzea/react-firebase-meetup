import React, { Component } from 'react';

class EventAttendee extends Component {
  render() {
    return (
      <img className="rounded-circle event-attendee-portrait" src='https://randomuser.me/api/portraits/women/42.jpg' alt=""/>
    )
  }
}

export default EventAttendee;
