import React, { Component } from 'react';
import { withTheme } from "@callstack/react-theme-provider";
import { NavLink } from 'react-router-dom';

class EventListOrganizer extends Component {
  render() {
    return (
      <div>
        <img className="rounded-circle event-organizer-portrait pull-left" src={ this.props.organizerData.photo_url } alt=""/>
        <p 
          className="event-organizer-name text-left"
          style={ this.props.theme.secondaryColor }
          ><NavLink 
            style={ this.props.theme.secondaryColor }
            to={ this.props.organizerData.link }
            >{ this.props.organizerData.full_name }
          </NavLink>
        </p>
        <a href={ this.props.organizerData.facebook } target="_blank">
          <i className="fa fa-facebook" aria-hidden="true"></i>
        </a>
        <a href={ this.props.organizerData.twitter } target="_blank">
          <i className="fa fa-twitter" aria-hidden="true"></i>
        </a>
        <a href={ this.props.organizerData.linkedin } target="_blank">
          <i className="fa fa-linkedin" aria-hidden="true"></i>
        </a>
      </div>
    )
  }
}

export default withTheme(EventListOrganizer);
