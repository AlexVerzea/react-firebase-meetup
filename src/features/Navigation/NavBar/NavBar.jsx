import React, { Component } from 'react'
import { Navbar, NavbarNav, NavbarToggler, Collapse, NavItem } from 'mdbreact';
import { NavLink } from 'react-router-dom';
import { withTheme } from "@callstack/react-theme-provider";

class NavBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            collapse: false,
            isWideEnough: false,
        };
        this.onClick = this.onClick.bind(this);
    }
    onClick() {
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    render() {
        return (
            <Navbar className="navbar navbar-main" style={ this.props.theme.backgroundImage } expand="lg" fixed="top">
                <NavItem>
                    <NavLink className="route-logo" to="/">
                        <i className="fa fa-meetup logo" style={ this.props.theme.primaryTransparentColor } aria-hidden="true"></i>
                    </NavLink>
                </NavItem>
                { !this.state.isWideEnough && <NavbarToggler style={ this.props.theme.primaryTransparentColor } onClick = { this.onClick } />}
                <Collapse isOpen = { this.state.collapse } navbar>
                    <NavbarNav left>
                        <NavItem>
                            <NavLink to="/events">
                                <button 
                                    style={ this.props.theme.primaryTransparentColor }
                                    className="button-2" 
                                >Events</button>
                                <button
                                    style={ this.props.theme.primaryTransparentColor }
                                    className="button-3" 
                                    onClick={ !this.state.isWideEnough ? this.onClick : "" }
                                >Events</button>
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink to="/organizers">
                                <button 
                                    style={ this.props.theme.primaryTransparentColor }
                                    className="button-2" 
                                >Organizers</button> 
                                <button
                                    style={ this.props.theme.primaryTransparentColor }
                                    className="button-3" 
                                    onClick={ !this.state.isWideEnough ? this.onClick : "" }
                                >People</button>
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink to="/createEvent">
                                <button 
                                    style={ this.props.theme.primaryTransparentColor }
                                    className="button-2" 
                                >Create Event</button>
                                <button 
                                    style={ this.props.theme.primaryTransparentColor }
                                    className="button-3" 
                                    onClick={ !this.state.isWideEnough ? this.onClick : "" }
                                >Create Event</button>
                            </NavLink>
                        </NavItem>
                    </NavbarNav>
                    <NavbarNav right>
                        <NavItem>
                        <button 
                            style={ this.props.theme.primaryTransparentColor }
                            className="button-2" 
                            onClick={() => { 
                                this.props.onChangeTheme(
                                    this.props.chooseNextTheme(
                                        this.props.theme.themeName, this.props.themes
                                    )
                                ); 
                            }}
                        >Theming</button>
                        <button 
                            style={ this.props.theme.primaryTransparentColor }
                            className="button-3" 
                            onClick={() => {
                                this.props.onChangeTheme(
                                    this.props.chooseNextTheme(
                                        this.props.theme.themeName, this.props.themes
                                    )
                                ); 
                            }}
                        >Theming</button>
                        </NavItem>
                        <NavItem>
                            <button 
                                style={ this.props.theme.primaryTransparentColor }
                                className="button-2" 
                            >Sign In</button>
                            <button 
                                style={ this.props.theme.primaryTransparentColor }
                                className="button-3" 
                                onClick={ !this.state.isWideEnough ? this.onClick : "" }
                            >Sign In</button>
                        </NavItem>
                        <NavItem>
                            <button 
                                style={ this.props.theme.primaryTransparentColor }
                                className="button-2" 
                            >Sign Up</button>
                            <button 
                                style={ this.props.theme.primaryTransparentColor }
                                className="button-3" 
                                onClick={ !this.state.isWideEnough ? this.onClick : "" }
                            >Sign Up</button>
                        </NavItem>
                    </NavbarNav>
                </Collapse>
            </Navbar>
        )
    }
}

export default withTheme(NavBar);
