// MDB React
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';

import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './app/layout/App';
import registerServiceWorker from './registerServiceWorker';

// The code below enables Hot Module Replacement through Webpack.
let render = () => {
    ReactDOM.render(<App />, document.getElementById('root'))
}
if (module.hot) {
    module.hot.accept('./app/layout/App', () => {
        setTimeout(render);
    })
}
render();
registerServiceWorker();
