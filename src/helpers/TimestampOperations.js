export const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

export function sortTimestampItems(unsortedTimestampItems, order) {

  // Sort Timestamps
  const unsortedTimestamps= [];
  unsortedTimestampItems.map((unsortedTimestampItem, i) => unsortedTimestamps.push(parseInt(unsortedTimestampItem.date, 10)));
  const sortedTimestamps = unsortedTimestamps.sort((x, y) => {
    return order === "FIFO" ? x - y : y - x;
  });

  // Sort timestampItems
  const sortedTimestampItems = [];
  sortedTimestamps.forEach((sortedTimestamp) => {
    sortedTimestampItems.push(unsortedTimestampItems.find((unsortedTimestampItem) => {
      return parseInt(unsortedTimestampItem.date, 10) === sortedTimestamp;
    }));
  });
  return sortedTimestampItems;
}

export function getFullDate(timestamp) { 
    return new Date(parseInt(timestamp, 10) * 1000);
}

export function formatDate(fullDate) {
    const year = fullDate.getFullYear();
    let date = fullDate.getDate();
    let month = fullDate.getMonth();
    if (date < 10) {
    date = '0' + date;
    } 
    if (month < 10) {
        month = '0' + month;
    } 
    return date + '/' + month + '/' + year;
}

export function formatTime(fullDate) {
    let hours = fullDate.getHours();
    let minutes = fullDate.getMinutes();
    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    return hours + ':' + minutes + ' ' + ampm;
}

export function formatTimeSince(fullDate) {
    var seconds = Math.floor((new Date() - fullDate) / 1000);
    var interval = Math.floor(seconds / 31536000);
    if (interval > 1) {
      return interval + " years ago";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
      return interval + " months ago";
    }
    interval = Math.floor(seconds / 604800);
    if (interval > 1) {
      return interval + " weeks ago";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
      return interval + " days ago";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
      return interval + " hours ago";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
      return interval + " minutes ago";
    }
    return Math.floor(seconds) + " seconds ago";
}