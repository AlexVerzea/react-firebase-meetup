export const events = [
    {
      "title": "Los Angeles Auto Show",
      "date": "1543586400",
      "location": "Los Angeles, California",
      "attendees": "1K+",
      "price": "50",
      "image_url": "/assets/events/events_pictures/event_2.jpg",
      "description": "One of the world's largest auto shows and 1,000 vehicles returns November 30 - December 9, 2018 at the LA Convention Center. Our show is your one-stop-shop for hassle-free comparison shopping cars, trucks, SUVs, electric vehicles and more. Whether it’s new vehicle shopping, free test drives, experiencing the latest tech or exploring an assortment of customized rides and exotics, there is something for everyone at the show.",
      "organizer": {
        "full_name": "Thomas Weller",
        "link": "/events",
        "photo_url": "/assets/events/organizers_pictures/organizer_2.jpg",
        "facebook": "https://www.facebook.com/",
        "twitter": "https://twitter.com/?lang=en",
        "linkedin": "https://www.linkedin.com/"
      }
    },
    {
      "title": "Tensorflow Dev Summit",
      "date": "1522414800",
      "location": "Mountain View, California",
      "attendees": "982",
      "price": "120",
      "image_url": "/assets/events/events_pictures/event_3.png",
      "description": "TensorFlow Dev Summit brings together a diverse mix of machine learning users from around the world for a full day of highly technical talks, demos, and conversation with the TensorFlow team and community.",
      "organizer": {
        "full_name": "Amanda Chen",
        "link": "/events",
        "photo_url": "/assets/events/organizers_pictures/organizer_3.jpg",
        "facebook": "https://www.facebook.com/",
        "twitter": "https://twitter.com/?lang=en",
        "linkedin": "https://www.linkedin.com/"
      }
    },
    {
      "title": "Apple Worldwide Developers Conference",
      "date": "1528117200",
      "location": "San Jose, California",
      "attendees": "1K+",
      "price": "150",
      "image_url": "/assets/events/events_pictures/event_1.large",
      "description": "When technology connects with creativity, incredible ideas come to life. This summer, we invite thousands of talented minds from around the world to join us and turn their ideas into reality.",
      "organizer": {
        "full_name": "John Smith",
        "link": "/events",
        "photo_url": "/assets/events/organizers_pictures/organizer_1.jpg",
        "facebook": "https://www.facebook.com/",
        "twitter": "https://twitter.com/?lang=en",
        "linkedin": "https://www.linkedin.com/"
      }
    },
    {
      "title": "International Conference on Quantum Computing",
      "date": "1543240800",
      "location": "Paris, France",
      "attendees": "832",
      "price": "75",
      "image_url": "/assets/events/events_pictures/event_4.jpg",
      "description": "The International Conference on Quantum Computing (ICoCQ) will take place at Ecole Normale Supérieure, Paris, from the 26th to the 30th of November, 2018. It will present an up to date perspective on the thriving field of quantum computing. The scientific program will combine invited tutorial talks by prominent specialists, contributed talks, poster presentations and a round-table discussion.",
      "organizer": {
        "full_name": "Sarah Anderson",
        "link": "/events",
        "photo_url": "/assets/events/organizers_pictures/organizer_4.jpg",
        "facebook": "https://www.facebook.com/",
        "twitter": "https://twitter.com/?lang=en",
        "linkedin": "https://www.linkedin.com/"
      }
    },
    {
      "title": "React Amsterdam",
      "date": "1523538000",
      "location": "Amsterdam, Netherlands",
      "attendees": "1K+",
      "price": "300",
      "image_url": "/assets/events/events_pictures/event_5.jpg",
      "description": "A full-day, two-track conference on all things React, gathering Front-end and Full-stack developers across the globe in the tech heart of Europe. Mark your calendars for the biggest React community event.",
      "organizer": {
        "full_name": "Mark de Jong",
        "link": "/events",
        "photo_url": "/assets/events/organizers_pictures/organizer_5.jpg",
        "facebook": "https://www.facebook.com/",
        "twitter": "https://twitter.com/?lang=en",
        "linkedin": "https://www.linkedin.com/"
      }
    },
    {
      "title": "Adobe Immerse",
      "date": "1528808400",
      "location": "Online",
      "attendees": "1K+",
      "price": "400",
      "image_url": "/assets/events/events_pictures/event_6.jpg",
      "description": "Global virtual conference for Enterprise Developers and Architects working with Adobe enterprise solutions.",
      "organizer": {
        "full_name": "Vanessa Adams",
        "link": "/events",
        "photo_url": "/assets/events/organizers_pictures/organizer_6.jpg",
        "facebook": "https://www.facebook.com/",
        "twitter": "https://twitter.com/?lang=en",
        "linkedin": "https://www.linkedin.com/"
      }
    }
]