export const updates = [
    {
      "organizer": {
        "full_name": "Sarah Anderson",
        "link": "/events",
      },
      "photo_url": "/assets/events/organizers_pictures/organizer_4.jpg",
      "action": " created a new ",
      "target": {
        "text": "event ",
        "link": "/events"
      },
      "punctuation": "!",
      "date": "1536843600"
    },
    {
      "organizer": {
        "full_name": "Amanda Chan",
        "link": "/events",
      },
      "photo_url": "/assets/events/organizers_pictures/organizer_3.jpg",
      "action": " created a new ",
      "target": {
        "text": "event ",
        "link": "/events"
      },
      "punctuation": "!",
      "date": "1522414800"
    },
    {
      "organizer": {
        "full_name": "Thomas Weller",
        "link": "/events",
      },
      "photo_url": "/assets/events/organizers_pictures/organizer_2.jpg",
      "action": " created a new ",
      "target": {
        "text": "event ",
        "link": "/events"
      },
      "punctuation": "!",
      "date": "1533218400"
    },
    {
      "organizer": {
        "full_name": "John Smith",
        "link": "/events",
      },
      "photo_url": "/assets/events/organizers_pictures/organizer_1.jpg",
      "action": " created a new ",
      "target": {
        "text": "event ",
        "link": "/events"
      },
      "punctuation": "!",
      "date": "1528117200"
    },
    {
      "organizer": {
        "full_name": "Vanessa Adams",
        "link": "/events",
      },
      "photo_url": "/assets/events/organizers_pictures/organizer_6.jpg",
      "action": " created a new ",
      "target": {
        "text": "event ",
        "link": "/events"
      },
      "punctuation": "!",
      "date": "1528808400"
    },
    {
      "organizer": {
        "full_name": "Mark de Jong",
        "link": "/events",
      },
      "photo_url": "/assets/events/organizers_pictures/organizer_5.jpg",
      "action": " created a new ",
      "target": {
        "text": "event ",
        "link": "/events"
      },
      "punctuation": "!",
      "date": "1523538000"
    }
]