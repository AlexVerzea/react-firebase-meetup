export function chooseNextTheme(currentTheme, themes) {
    let nextTheme = '';
    for (let i = 0; i < themes.length; i++) {
      if (themes[i] === currentTheme) {
        if (typeof themes[i+1] !== 'undefined') {
          nextTheme = themes[i+1];
        } else {
          nextTheme = themes[0];
        }
      }
    }
    if (nextTheme !== '') {
      return nextTheme;
    } else {
      return currentTheme;
    }
}