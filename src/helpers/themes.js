export const themes = {
    blue: {
        themeName: "blue",
        backgroundColor: {
            backgroundColor: "rgb(171, 188, 210)"
        },
        backgroundImage: {
            backgroundImage: "url(/assets/skylines/blue_skyline.jpg)"
        },
        primaryColor: {
            color: "rgb(32, 40, 73)"
        },
        primaryBackgroundColor: {
            backgroundColor: "rgb(32, 40, 73)"
        },
        primaryTransparentColor: {
            backgroundColor: "rgba(32, 40, 73, 0.8)"
        },
        primaryGradientColor: {
            backgroundImage: "linear-gradient(135deg, rgb(32, 40, 73) 0%, rgb(32, 40, 73) 70%, rgb(98, 130, 167) 90%)",
        },
        secondaryColor: {
            color: "rgb(252, 225, 122)"
        },
        secondaryColorBackground: {
            backgroundColor: "rgb(252, 225, 122)"
        },
        cardButton: {
            color: "rgb(32, 40, 73)",
            backgroundColor: "rgb(252, 225, 122)"
        }
    },
    red: {
        themeName: "red",
        backgroundColor: {
            backgroundColor: "rgb(248, 206, 204)"
        },
        backgroundImage: {
            backgroundImage: "url(/assets/skylines/red_skyline.jpg)",
        },
        primaryColor: {
            color: "rgb(144, 15, 22)"
        },
        primaryBackgroundColor: {
            backgroundColor: "rgb(144, 15, 22)"
        },
        primaryTransparentColor: {
            backgroundColor: "rgb(144, 15, 22, 0.8)",
        },
        primaryGradientColor: {
            backgroundImage: "linear-gradient(135deg, rgb(144, 15, 22) 0%, rgb(144, 15, 22) 70%, rgb(237, 56, 55) 90%)",
        },
        secondaryColor: {
            color: "rgb(252, 225, 122)"
        },
        secondaryColorBackground: {
            backgroundColor: "rgb(252, 225, 122)"
        },
        cardButton: {
            color: "rgb(172, 35, 54)",
            backgroundColor: "rgb(252, 225, 122)"
        }
    },
    orange: {
        themeName: "orange",
        backgroundColor: {
            backgroundColor: "rgb(242, 216, 178)"
        },
        backgroundImage: {
            backgroundImage: "url(/assets/skylines/orange_skyline.jpg)",
        },
        primaryColor: {
            color: "rgb(149, 56, 42)"
        },
        primaryBackgroundColor: {
            backgroundColor: "rgb(149, 56, 42)"
        },
        primaryTransparentColor: {
            backgroundColor: "rgba(149, 56, 42, 0.8)",
        },
        primaryGradientColor: {
            backgroundImage: "linear-gradient(135deg, rgb(149, 56, 42) 0%, rgb(149, 56, 42) 70%, rgb(238, 84, 40) 90%)",
        },
        secondaryColor: {
            color: "rgb(252, 225, 122)"
        },
        secondaryColorBackground: {
            backgroundColor: "rgb(252, 225, 122)"
        },
        cardButton: {
            color: "rgb(149, 56, 42)",
            backgroundColor: "rgb(252, 225, 122)"
        }
    },
    turquoise: {
        themeName: "turquoise",
        backgroundColor: {
            backgroundColor: "rgb(193, 229, 225)"
        },
        backgroundImage: {
            backgroundImage: "url(/assets/skylines/turquoise_skyline.jpg)",
        },
        primaryColor: {
            color: "rgb(33, 87, 91)"
        },
        primaryBackgroundColor: {
            backgroundColor: "rgb(33, 87, 91)"
        },
        primaryTransparentColor: {
            backgroundColor: "rgba(33, 87, 91, 0.8)",
        },
        primaryGradientColor: {
            backgroundImage: "linear-gradient(135deg, rgb(33, 87, 91) 0%, rgb(33, 87, 91) 70%, rgb(40, 168, 150)",
        },
        secondaryColor: {
            color: "rgb(252, 225, 122)"
        },
        secondaryColorBackground: {
            backgroundColor: "rgb(252, 225, 122)"
        },
        cardButton: {
            color: "rgb(33, 87, 91)",
            backgroundColor: "rgb(252, 225, 122)"
        }
    },
    purple: {
        themeName: "purple",
        backgroundColor: {
            backgroundColor: "rgb(197, 192, 227)"
        },
        backgroundImage: {
            backgroundImage: "url(/assets/skylines/purple_skyline.jpg)",
        },
        primaryColor: {
            color: "rgb(55, 49, 93)"
        },
        primaryBackgroundColor: {
            backgroundColor: "rgb(55, 49, 93)"
        },
        primaryTransparentColor: {
            backgroundColor: "rgba(55, 49, 93, 0.8)",
        },
        primaryGradientColor: {
            backgroundImage: "linear-gradient(135deg, rgb(55, 49, 93) 0%, rgb(55, 49, 93) 70%, rgb(105, 107, 147)",
        },
        secondaryColor: {
            color: "rgb(252, 225, 122)"
        },
        secondaryColorBackground: {
            backgroundColor: "rgb(252, 225, 122)"
        },
        cardButton: {
        color: "rgb(55, 49, 93)",
            backgroundColor: "rgb(252, 225, 122)"
        }
    }
};